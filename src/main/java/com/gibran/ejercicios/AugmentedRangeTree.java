package com.gibran.ejercicios;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;

/**
 * 
 * Improvements
 * 
 * - Make operations atomic (for now they are not);
 * - Improve tree reorganizing after merging the branches - for now just re-adding all children node which are not overlapped;
 * - Make it a Balanced tree (Red Back or AVL Tree) to improve the performance to O (log (n)) 
 * 
 * @author Gibran
 *
 */
public class AugmentedRangeTree {

    public Range root;
    
    /*
     * Will merge overlapped ranges  
     */
    public static AugmentedRangeTree mergeRanges (Range newRange, AugmentedRangeTree augmentedRangeTree) {
    	
    	Set<Range> overlappedRanges = new HashSet<>();
    	augmentedRangeTree.intersectRange(augmentedRangeTree.root, newRange, overlappedRanges);
    	// If there is no overlap just add the new range
    	if (overlappedRanges.size() == 0) {
    		augmentedRangeTree.root = AugmentedRangeTree.insertNode(augmentedRangeTree.root, newRange);
    	} else {
    		AtomicLong lowestNumber = new AtomicLong();
    		AtomicLong highestNumber = new AtomicLong();
    		Set<Range> RangesChildrenNodes = new HashSet<>();

    		boolean initialIteration = true;
    		for (Range existingRange : overlappedRanges) {
    			
    			// If it is the initial iteration set the numbers so in the next we can make the comparations
    			if (initialIteration) {
        			lowestNumber.getAndSet(existingRange.getStart() < newRange.getStart() ? existingRange.getStart() : newRange.getStart());
        			highestNumber.getAndSet(existingRange.getEnd() > newRange.getEnd() ? existingRange.getEnd() : newRange.getEnd());
        			initialIteration = false;
    			} else {
    				lowestNumber.getAndSet(existingRange.getStart() < lowestNumber.get() ? existingRange.getStart() : lowestNumber.get());
    				highestNumber.getAndSet(existingRange.getEnd() > highestNumber.get() ? existingRange.getEnd() : highestNumber.get());
    			}
    			
    			// Getting the children nodes so they can be reinserted
    			augmentedRangeTree.fillRangeSubtree(existingRange, RangesChildrenNodes, false, overlappedRanges);

    			// If the Range is the root, create a new tree
    			if (null == existingRange.isLeftNode()) {
    				augmentedRangeTree = new AugmentedRangeTree();
    			} else {
    				// Detach the node from the parent
    				if (existingRange.isLeftNode()) {
    					existingRange.getParent().setLeft(null);
    				} else {
    					existingRange.getParent().setRight(null);
    				}
    			}
    		}
    			// All the overlapping ranges where removed, now adding the children nodes from the removed nodes
    			for (Range subtreeRange: RangesChildrenNodes) {
    				augmentedRangeTree.root = AugmentedRangeTree.insertNode(augmentedRangeTree.root, new Range(subtreeRange.getStart(), subtreeRange.getEnd()));
    			}
    		
    		// Adding the new node with merged range
    		augmentedRangeTree.root = AugmentedRangeTree.insertNode(augmentedRangeTree.root, new Range(lowestNumber.get(), highestNumber.get()));
    	}
    	return augmentedRangeTree;
    }

    /*
     * Insert the node and save the Max value, parent value and position related to parent (left or right) to make it easy to detach the node when needed
     */
    private static Range insertNode(Range tempNode, Range newNode) {
        // If root node is null create it
    	if (tempNode == null) {
            tempNode = newNode;
            tempNode.setParent(null, null);
            return tempNode;
        }

        if (newNode.getEnd() > tempNode.getMax()) {
            tempNode.setMax(newNode.getEnd());
        }

        if (tempNode.compareTo(newNode) <= 0) {

            if (tempNode.getRight() == null) {
            	newNode.setParent(tempNode, false);
                tempNode.setRight(newNode);
            }
            else {
                insertNode(tempNode.getRight(), newNode);
            }
        } else {
            if (tempNode.getLeft() == null) {
                tempNode.setLeft(newNode);
                newNode.setParent(tempNode, true);
            } else {
                insertNode(tempNode.getLeft(), newNode);
            }
        }
        return tempNode;
    }

    /*
     * Count the numbers between the ranges
     */
    public static Long countRangesNumbers(Range tempNode, Long count) {
        if (tempNode == null) {
            return count;
        }

        if (tempNode.getLeft() != null) {
            count = countRangesNumbers(tempNode.getLeft(), count);
        }

        count += (tempNode.getEnd() + 1) - tempNode.getStart();

        if (tempNode.getRight() != null) {
        	count = countRangesNumbers(tempNode.getRight(), count);
        }
        
        return count;
    }

    /*
     * Save all the overlapped ranges inside a Set
     */
    private void intersectRange(Range tempNode, Range newRange, Set<Range> overlappedRanges) {

        if (tempNode == null) {
            return;
        }

        if (!((tempNode.getStart() > newRange.getEnd()) || (tempNode.getEnd() < newRange.getStart()))) {
            if (overlappedRanges == null) {
                overlappedRanges = new HashSet<Range>();
            }
            overlappedRanges.add(tempNode);
        }

        if ((tempNode.getLeft() != null) && (tempNode.getLeft().getMax() >= newRange.getStart())) {
            this.intersectRange(tempNode.getLeft(), newRange, overlappedRanges);
        }

        this.intersectRange(tempNode.getRight(), newRange, overlappedRanges);
    }
    
    private void fillRangeSubtree (Range Range, Set<Range> Ranges, boolean addRange, Set<Range> overlappedRanges) {
        if (Range.getLeft() != null) {
        	fillRangeSubtree(Range.getLeft(), Ranges, true, overlappedRanges);
        }

        if (addRange && !overlappedRanges.contains(Range)) {
        	Ranges.add(Range);
        }

        if (Range.getRight() != null) {
        	fillRangeSubtree(Range.getRight(), Ranges, true, overlappedRanges);
        }
    }
    
    private static void printTree(Range tempNode) {
        if (tempNode == null) {
            return;
        }

        if (tempNode.getLeft() != null) {
            printTree(tempNode.getLeft());
        }

        System.out.println(tempNode);

        if (tempNode.getRight() != null) {
            printTree(tempNode.getRight());
        }
    }
    
}
