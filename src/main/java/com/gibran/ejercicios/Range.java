package com.gibran.ejercicios;

class Range implements Comparable<Range> {

	private long     start;
    private long     end;
    private long     max;
    private Range left;
    private Range right;
    private Range parent;
    private Boolean leftNode;
    
    public long getStart() {
		return start;
	}

	public void setStart(long start) {
		this.start = start;
	}

	public long getEnd() {
		return end;
	}

	public void setEnd(long end) {
		this.end = end;
	}

	public long getMax() {
		return max;
	}

	public void setMax(long max) {
		this.max = max;
	}

	public Range getLeft() {
		return left;
	}

	public void setLeft(Range left) {
		this.left = left;
	}

	public Range getRight() {
		return right;
	}

	public void setRight(Range right) {
		this.right = right;
	}

    public Boolean isLeftNode() {
		return leftNode;
	}

	public Range(long start, long end) {

        this.start = start;
        this.end = end;
        this.max = end;
    }
 
    @Override
    public String toString() {
        return "[" + this.getStart() + ", " + this.getEnd() + ", "
               + this.getMax() + "]";
    }

    @Override
    public int compareTo(Range i) {
        if (this.start < i.start) {
            return -1;
        }
        else if (this.start == i.start) {
            return this.end <= i.end ? -1 : 1;
        }
        else {
            return 1;
        }
    }

    public Range getParent () {
    	return this.parent;
    }

    public void setParent (Range parent, Boolean leftNode) {
    	this.parent = parent;
    	this.leftNode = leftNode;
    }
    
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (end ^ (end >>> 32));
		result = prime * result + (int) (start ^ (start >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Range other = (Range) obj;
		if (end != other.end)
			return false;
		if (start != other.start)
			return false;
		return true;
	}
    
}