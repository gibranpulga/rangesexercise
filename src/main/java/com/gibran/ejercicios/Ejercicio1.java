package com.gibran.ejercicios;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;


/**
Problem:

El problema es el siguiente : el archivo adjunto(int_list.txt)  tiene números enteros del 1 a 1,000,000, puede haber repetidos y están desordenados. El archivo contiene un número por cada linea.

Consiste en encontrar dos pares de números que sumen estos números : 

231552,234756,596873,648219,726312,981237,988331,1277361,1283379

Si algún número no se puede encontrar simplemente especificarlo.

 * I created 2 different solutions and one implementation using parallel streams. One solution uses a hashset to search for 
 * the second number of the pair, which should be more efficient than the other one, which uses a binary tree, since 
 * the binary tree one has to sort the list. Also I created one using parallel streams just to play around a bit with it.
 * I am using Java 8 features because I am studying them and forcing myself to use them in these algorithms to
 * delve further into them and functional programming.  
 *
 */
public class Ejercicio1 {

	public static void main(String[] args) throws IOException {
		final List<Integer> desiredSums = Arrays.asList(231552,234756,596873,648219,726312,981237,988331,1277361,1283379);
		String fileName = args.length == 0 ? "int_list.txt" : args[0];
		// Some repetitions to warm the VM for the time to be a bit more realistic and some time benchmark to have a reference
		for (int i = 0; i < 20; i++) {
			new Ejercicio1().findSumAddendsWithBinarySearch(fileName, desiredSums);
			new Ejercicio1().findSumAddendsWithHash(fileName, desiredSums);
			findSumAddendsWithParallelStream(fileName);
		}
	}
	
	public Map<Integer, Map<Integer, Integer>> findSumAddendsWithHash (String fileName, List<Integer> desiredSums) throws IOException {
		long init = System.currentTimeMillis();
		// For testing purposes
		Map<Integer, Map<Integer, Integer>> sumExistentPairs = new HashMap<>();

		// This will take O(n) I think
		Set<Integer> numbers = Files.lines(Paths.get(fileName)).mapToInt(y -> Integer.parseInt(y)).boxed().collect(Collectors.toSet());
		desiredSums.forEach(desiredSum -> {
			// This is ugly yes, there are better ways of doing it, but I just wanted a quick way of saving the repetitions and in the end didn't have time to refactor it 
			int[] repetitions = new int[]{0};
			Map<Integer, Integer> pairs = new LinkedHashMap<>();
		
			// peek() is not the better solution but as I said I am still learning Java 8 features, so this was the way I found 
			// with anymatch() to break the foreach after 2 repetitions were found
			numbers.stream().peek(number -> {
				// This will take O(1)
				if (numbers.contains((desiredSum - number))) {
					// This allows for the list to contain duplicates
					if (null == pairs.get(desiredSum - number) && null == pairs.put(Integer.valueOf(number), (desiredSum - number))) {
						repetitions[0]++;
					};
				}
				if (repetitions[0] == 2) {
					System.out.println("Found 2 pairs that have the sum " + desiredSum + " : pair 1: " 
																			+ pairs.keySet().toArray()[0] + " and " + pairs.values().toArray()[0]
																			+ " , pair 2: "
																			+ pairs.keySet().toArray()[1] + " and " + pairs.values().toArray()[1]);
					sumExistentPairs.put(desiredSum, pairs);
				}
			}).anyMatch(until -> repetitions[0] == 2);
			if (repetitions[0] < 2) {
				System.out.println("There are not 2 pairs of numbers that sum to this value: " + desiredSum);
			}
			repetitions[0] = 0;
			
		});
		System.out.println("Finding the addends with hash solution took " + (System.currentTimeMillis() - init) + "ms");
		return sumExistentPairs;
		
	}

	/**
	 * Using binary search to look for the sum
	 * @param fileName
	 * @param desiredSums
	 * @return
	 * @throws IOException
	 */
	public Map<Integer, Map<Integer, Integer>> findSumAddendsWithBinarySearch (String fileName, List<Integer> desiredSums) throws IOException {
		long init = System.currentTimeMillis();
		// For testing purposes
		Map<Integer, Map<Integer, Integer>> sumExistentPairs = new HashMap<>();
		// This will take O(n log(n))
		// In this case the list doesn't need to be distinct since I will add the pair values to a hash
		List<Integer> numbers = Files.lines(Paths.get(fileName)).mapToInt(y -> Integer.parseInt(y)).sorted().boxed().collect(Collectors.toList());
		desiredSums.forEach( desiredSum -> { 
				int[] repetitions = new int[]{0};
				Map<Integer, Integer> pairs = new LinkedHashMap<>();
				numbers.stream().peek(number -> {
					// This will take O(log(n))
					if (Collections.binarySearch(numbers, desiredSum - number) > 0) {
						// This allows for the list to contain duplicates
						if (null == pairs.get(desiredSum - number) && null == pairs.put(Integer.valueOf(number), desiredSum - number)) {
							repetitions[0]++;
						};
					}
					if (repetitions[0] == 2) {
						System.out.println("Found 2 pairs that have the sum " + desiredSum + " : pair 1: " 
																				+ pairs.keySet().toArray()[0] + " and " + pairs.values().toArray()[0]
																				+ " , pair 2: "
																				+ pairs.keySet().toArray()[1] + " and " + pairs.values().toArray()[1]);
						sumExistentPairs.put(desiredSum, pairs);
					}
				}).anyMatch(until -> repetitions[0] == 2);
				if (repetitions[0] < 2) {
					System.out.println("There are not 2 pairs of numbers that sum to this value: " + desiredSum);
				}
				repetitions[0] = 0;
			});
		System.out.println("Finding the addends with binary search solution took " + (System.currentTimeMillis() - init) + "ms");

		return sumExistentPairs;
	}

	/**
	 * Created this just to play a bit with parallel streams
	 * @throws IOException
	 */
	public static void findSumAddendsWithParallelStream(String fileName) throws IOException {
		long init = System.currentTimeMillis();

		Set<Integer> numbers = Files.lines(Paths.get(fileName)).mapToInt(y -> Integer.parseInt(y)).boxed().collect(Collectors.toSet());

		int[] desiredSums = new int[] { 231552, 234756, 596873, 648219, 726312, 981237, 988331, 1277361, 1283379 };

		Map<Integer, Integer> matches = Collections.synchronizedMap(new HashMap<>());

		// Will iterate over the whole array but just once
		numbers.parallelStream().forEach(number -> {
			for (int sum : desiredSums) {
				if (sum >= number) {

					if (numbers.contains(sum - number)) {
						// Need to synchronize the map 
						synchronized (matches) {
							Integer previous = matches.get(sum);

							// If there was already an entry for this sum before, let's check if the value is not -1, if it is it means that this sum has already 2 pairs
							// If not, it means it has one, so we'll set the value to -1 (we're after 2 pairs only after all)
							if (previous != null) {
								if (previous != -1) {
									System.out.println("Pairs for " + sum);
									System.out.println(String.format("%d + %d = %d", number, sum - number, sum));
									System.out.println(String.format("%d + %d = %d", previous, sum - previous, sum));
									matches.put(sum, -1);
								}
							} else {
								matches.put(sum, number);
							}
						}
					}
				}
			}
		});

		// Now listing the values where there was not 2 pairs of numbers that sum to it
		for (int sum : desiredSums) {
			if (matches.getOrDefault(sum, 0) != -1) {
				System.out.println("There are not 2 pairs of numbers that sum to this value:" + sum);
			}
		}

		System.out.println("Finding the addends with parallel solution took " + (System.currentTimeMillis() - init) + "ms");
	}

}
