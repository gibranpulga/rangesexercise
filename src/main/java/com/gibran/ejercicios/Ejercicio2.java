package com.gibran.ejercicios;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.IntStream;


/**

Problem:

Como input tienes un fichero en el que cada línea especifica un rango de números consecutivos definido por el número inicial y final [a b]  donde a < b . Por ejemplo [2 ,  5] especifica los números 2,3,4,5 .
El ejercicio es calcular la cantidad de números distintos que estan comprendidos en la unión de todos los rangos definidos en el fichero, teniendo en cuenta que los rangos pueden estar solapados o incluidos en otros rangos. Los números pueden estar comprendidos entre 0 y 2^31.

Por ejemplo, dados estos rangos : 
[1 4] 
[6 10]
[13 , 15]
[2 ,4]

La respuesta sería 12, ya que hay 12 números comprendidos entre todos los rangos:  1 , 2 , 3, 4 , 6 , 7, 8 , 9 , 10 , 13, 14, 15 


 * I have created two solutions, a simple and "elegant" one using Java 8 features and an efficient one using an array of bits within an array of int
 *
 */
public class Ejercicio2 {
	private AugmentedRangeTree augmentedRangeTree = new AugmentedRangeTree();
	
	public static void main(String[] args) throws IOException {
		String fileName1 = args.length == 0 ? "range_small.txt" : args[0];
		String fileName2 = args.length == 0 ? "range_large.txt" : args[0];
		String fileName3 = args.length == 0 ? "range_mid2.txt" : args[0];
		String fileName4 = args.length == 0 ? "range_mid3.txt" : args[0];
		//new Ejercicio2().findNumbersInRageElegantly(fileName1);
		new Ejercicio2().findNumbersRangeWithinRange(fileName1);
		new Ejercicio2().findNumbersInRageEfficiently(fileName1);
		new Ejercicio2().findNumbersWithAugmentedRangeTree(fileName1);
		//new Ejercicio2().findNumbersInRageElegantly(fileName3);
	}
	
	/**
	 * This is an elegant solution with Java 8 features, and it will even be faster than the efficient one for files up to a size.
	 * However is not very efficient for large files like
	 * range_large.txt since it will require a long space in memory (32 bits for each int). It will actually cause OutOfMemory with default VM arguments
	 * @throws IOException
	 */
    public long findNumbersInRageElegantly(String fileName) throws IOException {
    	long start = System.currentTimeMillis();
        long total = Files.lines(Paths.get(fileName))
                .flatMapToInt(l -> toRange(l))
                .distinct()
                .count();

        System.out.println("Numbers within all ranges union = " + total);
        System.out.printf("Time in miliseconds: %d%n", System.currentTimeMillis() - start);
        return total;
    }
    
    public static IntStream toRange(String value) {
        String[] vals = value.split(" ");
        return IntStream.rangeClosed(Integer.parseInt(vals[0]), Integer.parseInt(vals[1]));
    }

    /**
     * This is a more efficiente solution since it will take 32x less space in memory
     * Basically I'm using the bits inside an int to mark each number as existent or not
     * @throws IOException
     */
    public long findNumbersInRageEfficiently(String fileName) throws IOException {
    	long start = System.currentTimeMillis();
        int[] numbersWithinRange = new int[Integer.MAX_VALUE / 32 + 1];
        // Using this set to avoid always iterate over the whole array
        Set<Integer> usedPositions = new HashSet<>();
        Files.lines(Paths.get(fileName))
        		.map(lines -> lines.split("\n")).flatMap(Arrays::stream)
        		// this will have unexpected behavior since operations are not atomic
        		//.parallel()
                .forEach(pairs -> {
                    String[] pairValues = pairs.split(" ");
                    for (int numberInRange = Integer.parseInt(pairValues[0]); numberInRange <= Integer.parseInt(pairValues[1]); numberInRange++) {
                    	// This is where the magic happens, each int can mark up to 32 numbers, so for each range of 32 numbers n / 32 will
                    	// give me the right position on the array, and then I�m usin a bitshift operation to mark the bit related to the 
                    	// current number of the range. 
                    	// Then I use a Or operation to keep the existing bits marked
                    	// After this all I need to do is a bit count 
                        numbersWithinRange[numberInRange / 32] = (int) (numbersWithinRange[numberInRange / 32] | (1 << (numberInRange % 32)));
                        // Adding only the used positions of the array so I don't have to iterate over whole array
                        usedPositions.add(numberInRange / 32);
                    };
                });

        long total = 0;

        for (Integer slotWithMarkedNumber: usedPositions) {
        	total += Integer.bitCount((numbersWithinRange[slotWithMarkedNumber]));
        }
        System.out.println("Numbers within all ranges (using array of bits) = " + total);
        System.out.printf("Time in miliseconds: %d%n", System.currentTimeMillis() - start);
        
        return total;
    }
    
	 /**
     * This solution merges ranges that are within each other, using brute force (iterating over all of them)
     * 
     * @throws IOException
     */
    public long findNumbersRangeWithinRange (String fileName ) throws IOException {
    	long start = System.currentTimeMillis();
    	Map <Integer, Integer> pairs = new ConcurrentHashMap<>();
        Files.lines(Paths.get(fileName))
									.map(lines -> lines.split("\n")).flatMap(Arrays::stream)
									.parallel()
									.forEach(linePairs -> {
										String[] pairValues = linePairs.split(" ");
										mergeRanges(pairs, pairValues);
									});
        AtomicLong count = new AtomicLong() ;
        pairs.entrySet().parallelStream().forEach(pair -> count.addAndGet((pair.getValue() + 1) - pair.getKey()) );

        System.out.println("Numbers within all ranges (using within ranges) = " + count);
        System.out.printf("Time in miliseconds: %d%n", System.currentTimeMillis() - start);
        
        return count.get();
    }
    
    private void mergeRanges (Map<Integer, Integer> pairs, String[] pairValues) {
    	for (Iterator<Entry<Integer, Integer>> iterator = pairs.entrySet().iterator(); iterator.hasNext(); ) {
    		Entry<Integer, Integer> pair = iterator.next();
    		// so when the recursive function is called it skips the record itself
    		if (Integer.valueOf(pairValues[0]).equals(pair.getKey()) && Integer.valueOf(pairValues[1]).equals(pair.getValue()) ) {
    			continue;
    		}
    		if (Integer.valueOf(pairValues[0]) <= pair.getValue() && Integer.valueOf(pairValues[1]) >= pair.getKey() ) {
    			Integer newFirstValue = Integer.valueOf(pairValues[0]) <= pair.getKey() ? Integer.valueOf(pairValues[0]) : pair.getKey();
    			Integer newSecondValue = Integer.valueOf(pairValues[1]) >= pair.getValue() ? Integer.valueOf(pairValues[1]) : pair.getValue();
    			// atomic operations
    			pairs.computeIfPresent(Integer.valueOf(pairValues[0]), (key, value) -> null);
    			pairs.computeIfPresent(pair.getKey(), (key, value) -> null);
    			pairs.putIfAbsent(newFirstValue, newSecondValue);
    			String[] newRange = new String [] { newFirstValue.toString(), newSecondValue.toString() } ;
    			// need to be recursive since the new range might now overlap with another existing one
    			mergeRanges(pairs, newRange);
    			return;
    		}
    	}
		pairs.putIfAbsent(Integer.valueOf(pairValues[0]), Integer.valueOf(pairValues[1]));
    }

		 /**
     * This solution uses Augmented Interval Tree (unbalanced) to merge the ranges
     * 
     * @throws IOException
     */

    public long findNumbersWithAugmentedRangeTree (String fileName) throws IOException {
    	long start = System.currentTimeMillis();
        Files.lines(Paths.get(fileName))
									.map(lines -> lines.split("\n")).flatMap(Arrays::stream)
									.forEach(linePairs -> {
										String[] pairValues = linePairs.split(" ");
										augmentedRangeTree = AugmentedRangeTree.mergeRanges(new Range(Integer.valueOf(pairValues[0]), Integer.valueOf(pairValues[1])), augmentedRangeTree);
									});
        AtomicLong count = new AtomicLong() ;
        count.getAndSet(AugmentedRangeTree.countRangesNumbers(augmentedRangeTree.root, 0L));

        System.out.println("Numbers within all ranges (using augmented Range Tree) = " + count);
        System.out.printf("Time in miliseconds: %d%n", System.currentTimeMillis() - start);
        
        return count.get();
    }
}
