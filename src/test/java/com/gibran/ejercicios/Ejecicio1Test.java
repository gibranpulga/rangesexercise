package com.gibran.ejercicios;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.junit.Test;

/**
 * Yes, I know, quite simple tests, I haven't had the time to build more tests. 
 * These where more meant to help me refactor the code.
 */
public class Ejecicio1Test {

	@Test
	public void shouldReturn2CorectPairsForRangeWithHash() throws IOException {
		final List<Integer> desiredSums = Arrays.asList(5, 10, 12);
		String content = "1\n4\n3\n2\n1\n4\n5\n5\n7\n";
		String fileName = "testFile.txt";
		Files.write(Paths.get(fileName), content.getBytes());
		Map<Integer, Map<Integer,Integer>> result = new Ejercicio1().findSumAddendsWithHash(fileName, desiredSums);
		assertEquals("Result should contains 2 entries (5 and 10).", 2, result.size());
		for (Entry<Integer, Map<Integer, Integer>> entry: result.entrySet()) {
			for (Entry<Integer, Integer> pair: entry.getValue().entrySet()) {
				assertEquals("Sum of the pairs must match.", Integer.valueOf(entry.getKey()), Integer.valueOf(pair.getKey() + pair.getValue()));
			}
		}
		Files.delete(Paths.get(fileName));
	}
	
	@Test
	public void shouldReturn2CorectPairsForRangeWithBinarySearch() throws IOException {
		final List<Integer> desiredSums = Arrays.asList(5, 10, 12);
		String content = "1\n4\n3\n2\n1\n4\n5\n5\n7\n";
		String fileName = "testFile.txt";
		Files.write(Paths.get(fileName), content.getBytes());
		Map<Integer, Map<Integer,Integer>> result = new Ejercicio1().findSumAddendsWithBinarySearch(fileName, desiredSums);
		assertEquals("Result should contains 2 entries (5 and 10).", 2, result.size());
		for (Entry<Integer, Map<Integer, Integer>> entry: result.entrySet()) {
			for (Entry<Integer, Integer> pair: entry.getValue().entrySet()) {
				assertEquals("Sum of the pairs must match.", Integer.valueOf(entry.getKey()), Integer.valueOf(pair.getKey() + pair.getValue()));
			}
		}
		Files.delete(Paths.get(fileName));
	}
}
