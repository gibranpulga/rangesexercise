package com.gibran.ejercicios;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.junit.Test;

/**
 * Yes, I know, quite simple tests, I haven't had the time to build more tests. 
 * These where more meant to help me refactor the code.
 */
public class Ejercicio2Test {

	@Test
	public void shouldReturn12ForRange() throws IOException {
		String content = "1 4\n6 10\n13 15\n2 4";
		String fileName = "testFile.txt";
		Files.write(Paths.get(fileName), content.getBytes());
		//assertEquals("Should return 12 for this range.", 12, new Ejercicio2().findNumbersInRageEfficiently(fileName));
		//assertEquals("Should return 12 for this range.", 12, new Ejercicio2().findNumbersInRageElegantly(fileName));
		assertEquals("Should return 12 for this range.", 12, new Ejercicio2().findNumbersRangeWithinRange(fileName));
		assertEquals("Should return 12 for this range.", 12, new Ejercicio2().findNumbersWithAugmentedRangeTree(fileName));
		Files.delete(Paths.get(fileName));
	}
}
